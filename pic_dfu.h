////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//////
//////   pic_dfu.c 
//////
//////         Driver code for "DFU" device.  The "DFU" device is responsible
//////         for handling PIC "Device Firmware Upload" tasks.  In other
//////         words, the DFU device handles PIC firmware upgrades.  This
//////         device should only be created if it is determined that the
//////         DFU USB device class (bootsector) is currently running at
//////         enumeration time.
//////
//////                   Mohammad Jamal Mohiuddin
//////                   NCR, Inc
//////                   07/25/2018
//////
//////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//
/*
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */
#ifndef PIC_DFU_H
#define PIC_DFU_H

struct dfu_status
{
	unsigned char  status;
	unsigned char  poll_timeout[3];
	unsigned char  state;
	unsigned char  string;
};

#define POSC_EP_DFU         0           // DFU uses control endpoint
#define USB_INTERFACE_CLASS_APPLICATION_SPECIFIC_INTERFACE      0xFE
#define USB_INTERFACE_SUBCLASS_DFU                              0x01

//------------------------------------
// DFU Class-Specific Request Values
// Table 3.2 Page 9 of DFU spec rev1.0
//------------------------------------
#define DFU_DETACH    0
#define DFU_DNLOAD    1
#define DFU_UPLOAD    2
#define DFU_GETSTATUS 3
#define DFU_CLRSTATUS 4
#define DFU_GETSTATE  5
#define DFU_ABORT     6

//-----------------------------------
// DFU Device Status
// Page 19 of in DFU spec rev1.0
// Used by variable : DFUDeviceStatus
//-----------------------------------
#define DFU_STATUS_OK              0x00
#define DFU_STATUS_ERRTARGET       0x01
#define DFU_STATUS_ERRFILE         0x02
#define DFU_STATUS_ERRWRITE        0x03
#define DFU_STATUS_ERRERASE        0x04
#define DFU_STATUS_ERRCHECK_ERASED 0x05
#define DFU_STATUS_ERRPROG         0x06
#define DFU_STATUS_ERRVERIFY       0x07
#define DFU_STATUS_ERRADDRESS      0x08
#define DFU_STATUS_ERRNOTDONE      0x09
#define DFU_STATUS_ERRFIRMWARE     0x0A
#define DFU_STATUS_ERRVENDOR       0x0B
#define DFU_STATUS_ERRUSBR         0x0C
#define DFU_STATUS_ERRPOR          0x0D
#define DFU_STATUS_ERRUNKNOWN      0x0E
#define DFU_STATUS_ERRSTALLEDPKT   0x0F

//----------------------------------
// DFU Device State
// Page 20 of in DFU spec rev1.0
// Used by variable : DFUDeviceState
//----------------------------------
#define DFU_STATE_APPIDLE               0
#define DFU_STATE_APPDETACH             1
#define DFU_STATE_DFUIDLE               2
#define DFU_STATE_DFUDNLOADSYNC         3
#define DFU_STATE_DFUDNBUSY             4
#define DFU_STATE_DFUDNLOADIDLE         5
#define DFU_STATE_DFUMANIFESTSYNC       6
#define DFU_STATE_DFUMANIFEST           7
#define DFU_STATE_DFUMANIFESTWAITRESET  8
#define DFU_STATE_DFUUPLOADIDLE         9
#define DFU_STATE_DFUERROR             10


//-------------------------
// Reserved BlockNum Values
//-------------------------
#define DFU_BLKNUM_ERASE_ALL 0xFFFF
#define DFU_BLKNUM_ERASE_S1  0xFFFE
#define DFU_BLKNUM_ERASE_S2  0xFFFD


#endif
