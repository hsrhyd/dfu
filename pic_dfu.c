////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////
////   pic_dfu.c
////
////         Driver code for "DFU" device.  The "DFU" device is responsible
////         for handling PIC "Device Firmware Upload" tasks.  In other
////         words, the DFU device handles PIC firmware upgrades.  This
////         device should only be created if it is determined that the
////         DFU USB device class (bootsector) is currently running at
////         enumeration time.
////
////                   Mohammad Jamal Mohiuddin
////                   NCR, Inc
////                   07/25/2018
////
////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

/*
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */
#include <linux/module.h>
#include <linux/usb.h>
#include <linux/slab.h>
#include <linux/kthread.h>
#include "pic_dfu.h"
#include "PIC_Access_Macros.h"
#include "pic_firmware.h"

#define DRIVER_VERSION  "v0.1"
#define DRIVER_AUTHOR   "Mohammad Jamal Mohiuddin <mm185399@ncr.com>"
#define DRIVER_DESCRIPTION  "DFU Driver for MIB Panther Board"
#define USB_MIB_VENDOR_ID       0x07d5
#define USB_MIB_PRODUCT_ID      0x04ca
#define GET_STATUS_MAX_ITERATIONS 3

struct dfu_device
{
    struct usb_device *usb_dev;
    struct usb_interface *interface;
    wait_queue_head_t download_wait;
    int dfu_device_removed;
    struct task_struct *download_thread;
    struct mutex download_mutex;
};

static const struct usb_device_id pic_dfu_table[] = {
        { USB_DEVICE(USB_MIB_VENDOR_ID, USB_MIB_PRODUCT_ID) },
        { }
};

/*++

    dfu_delete

Routine Description:

    Frees all data structures allocated and stops the download thread

Arguments:

    dev - Pointer to device context

--*/

void dfu_delete(struct dfu_device *dev)
{
    int ret;
    if (dev) {
        ret = kthread_stop(dev->download_thread);
        dev_info(&dev->interface->dev, "%s:kthread finish with status:%d\n",
                 __func__, ret);
        kfree(dev);
    }
}

/*++

    is_dfu_interface

Routine Description:

    Checks whether the passed interface is DFU Interface or not

Arguments:

    iface_desc - Interface Descriptor

Return Value:

    Returns 1 if the interface is DFU else 0

--*/

int is_dfu_interface(struct usb_host_interface *iface_desc)
{
    if (!iface_desc)
        return 0;

    if ((USB_INTERFACE_CLASS_APPLICATION_SPECIFIC_INTERFACE
                                        == iface_desc->desc.bInterfaceClass)
         && (USB_INTERFACE_SUBCLASS_DFU == iface_desc->desc.bInterfaceSubClass))
        return 1;

    return 0;
}

/*++

    issue_control_request

Routine Description:

    Issues a USB Control message to the respective pipe

Arguments:

    dev - Pointer to Device Context
    pipe - endpoint pipe to send the message to sndctrl/rcvctrl
    bRequestType - USB message request type value
    bRequest - USB message request value
    wValue - USB message value
    wIndex - USB message index value
    wLength - length in bytes of the data to send
    buffer - pointer to the data to send
    bytes_transferred - pointer to store actual bytes transferred

Return Value:

    If successful, returns 0. Otherwise, a negative error number

--*/

static int issue_control_request(struct dfu_device *dev,
                unsigned int pipe,
                unsigned char bRequestType,
                unsigned char bRequest,
                unsigned short wValue,
                unsigned short wIndex,
                unsigned short wLength,
                void *buffer,
                int *bytes_transferred)
{
    int retval = -EINVAL;

    if (!dev || !bytes_transferred)
        return retval;

    retval = usb_control_msg(dev->usb_dev,
                pipe,
                bRequest,
                bRequestType,
                wValue,
                wIndex,
                buffer,
                wLength,
                USB_CTRL_SET_TIMEOUT);
    dev_dbg(&dev->interface->dev, "%s: RequestType:%d\t retval:%d\n",
             __func__, bRequestType, retval);
    if (retval >= 0) {
        *bytes_transferred = retval;
        return 0;
    }
    return retval;
}

/*++

    issue_get_dfu_state_usb_req

Routine Description:

    Issues "DFU State" request to PIC

Arguments:

    dev - Pointer to device structure
    dfu_state - Pointer in which DFU State will be stored on success

Return Value:

    TRUE on success else FAIL

--*/

static bool issue_get_dfu_state_usb_req(struct dfu_device *dev,
                                        unsigned char *dfu_state)
{
    int retval;
    int bytes_transferred;

    if (!dev || !dfu_state)
        return false;

    retval = issue_control_request(dev,
                     usb_rcvctrlpipe(dev->usb_dev, 0),
                     USB_DIR_IN | USB_TYPE_CLASS | USB_RECIP_INTERFACE,
                     DFU_GETSTATE,
                     0,
                     0,
                     sizeof(*dfu_state),
                     dfu_state,
                     &bytes_transferred);

    return (retval) ? false : true;
}

/*++

    issue_get_dfu_status_usb_req

Routine Description:

    Issues "DFU Status" request to PIC

Arguments:

    dev - Pointer to device structure
    dfu_status - Pointer in which DFU Status will be stored on success

Return Value:

    TRUE on success else FAIL

--*/

static bool issue_get_dfu_status_usb_req(struct dfu_device *dev,
                                         struct dfu_status *dfu_status)
{
    int retval;
    int bytes_transferred;

    if (!dev || !dfu_status)
        return false;

    retval = issue_control_request(dev,
                     usb_rcvctrlpipe(dev->usb_dev, 0),
                     USB_DIR_IN | USB_TYPE_CLASS | USB_RECIP_INTERFACE,
                     DFU_GETSTATUS,
                     0,
                     0,
                     sizeof(*dfu_status),
                     dfu_status,
                     &bytes_transferred);

    return (retval) ? false : true;
}

/*++

    issue_dfu_abort_usb_req

Routine Description:

    Issues "DFU Abort" request to PIC

Arguments:

    dev - Pointer to device structure

Return Value:

    TRUE on success else FAIL

--*/

static bool issue_dfu_abort_usb_req(struct dfu_device *dev)
{
    int retval;
    int bytes_transferred;

    if (!dev)
        return false;

    retval = issue_control_request(dev,
                     usb_sndctrlpipe(dev->usb_dev, 0),
                     USB_DIR_OUT | USB_TYPE_CLASS | USB_RECIP_INTERFACE,
                     DFU_ABORT,
                     0,
                     0,
                     0,
                     NULL,
                     &bytes_transferred);

    return (retval) ? false : true;
}

/*++

    issue_dfu_clear_status_usb_req

Routine Description:

    Issues "DFU Clear Status" request to PIC

Arguments:

    dev - Pointer to device structure

Return Value:

    TRUE on success else FAIL

--*/

static bool issue_dfu_clear_status_usb_req(struct dfu_device *dev)
{
    int retval;
    int bytes_transferred;

    if (!dev)
        return false;

    retval = issue_control_request(dev,
                     usb_sndctrlpipe(dev->usb_dev, 0),
                     USB_DIR_OUT | USB_TYPE_CLASS | USB_RECIP_INTERFACE,
                     DFU_CLRSTATUS,
                     0,
                     0,
                     0,
                     NULL,
                     &bytes_transferred);

    return (retval) ? false : true;
}

/*++

    issue_dfu_download_usb_req

Routine Description:

    Issues "DFU Download" command to PIC with corresponding
    data and block Number

Arguments:

    dev - Pointer to device structure
    block_num - Block Number
    buffer - Pointer to data buffer
    buffer_size - size of the buffer

Return Value:

    TRUE on success else FAIL

--*/

static bool issue_dfu_download_usb_req(struct dfu_device *dev,
                                       unsigned short block_num,
                                       unsigned char *buffer,
                                       unsigned char buffer_size)
{
    int retval;
    int bytes_transferred;

    if (!dev)
        return false;

    retval = issue_control_request(dev,
                     usb_sndctrlpipe(dev->usb_dev, 0),
                     USB_DIR_OUT | USB_TYPE_CLASS | USB_RECIP_INTERFACE,
                     DFU_DNLOAD,
                     block_num,
                     0,
                     buffer_size,
                     buffer,
                     &bytes_transferred);
    return (retval) ? false : true;
}

/*++

    download_program_device

Routine Description:

    Flashes the firmware down to the device and have the device
    reset when complete

Arguments:

    dev - Pointer to device structure

Return Value:

    TRUE on success else FAIL

--*/

static bool download_program_device(struct dfu_device *dev)
{
    bool retval = false;
    unsigned short checksum;
    unsigned int i;
    unsigned int j;
    unsigned int k;
    unsigned int iterations;
    bool firmware_send;
    unsigned char download_buffer[MAX_DOWNLOAD_BLOCK_SIZE];
    struct dfu_status dfu_status;
    unsigned char dfu_state;
    unsigned int download_block_size;
    unsigned int app_sector_size;
    unsigned int app_sector_start_address;
    unsigned char *firmware_image;
    unsigned short firmware_checksum;
    unsigned long firmware_size;
    dev_dbg(&dev->interface->dev, "%s\n", __func__);

    if (!dev)
        return false;

    // Currently only supporting 18F66J50 PIC
    download_block_size      = DOWNLOAD_BLOCK_SIZE_PIC18F66J50;
    app_sector_size          = PIC18F66J50_APPSECTOR_SIZE;
    firmware_image           = (unsigned char *) ucPICAppsectorFirmware;
    app_sector_start_address = PIC_APPSECTOR_START_ADDRESS;
    firmware_size = app_sector_size / sizeof(unsigned short) - 1;
    firmware_checksum = ((unsigned short *)firmware_image)[firmware_size];
    checksum = RADPC_DOWNLOAD_CHECKSUM_SEED_VALUE;

    mutex_lock_interruptible(&dev->download_mutex);
    for (k = 0; k < (app_sector_size - sizeof(unsigned short)); k++)
        checksum += (firmware_image[k]);
    if (firmware_checksum != checksum) {
        dev_err(&dev->interface->dev, "%s: Checksum stored in embedded "
                "firmware array is invalid (%04x,%04x)\n", __func__,
                firmware_checksum, checksum);
        goto program_device_cleanup;
    }

    for (i = 0; i < (app_sector_size / download_block_size); i++) {
        firmware_send = false;
        for (j = 0; j < download_block_size; j++) {
            if (firmware_image[(i * download_block_size) + j] != 0xff) {
                firmware_send = true;
                break;
            }
        }
        if (!firmware_send)
            continue;

        memcpy(download_buffer, &firmware_image[i * download_block_size],
               download_block_size);
        if (!issue_dfu_download_usb_req(dev,
                        (unsigned short)i,
                        download_buffer,
                        (unsigned char)download_block_size)) {
            dev_err(&dev->interface->dev, "%s: DFU Download failed block=0x%x "
                    "blocksize=0x%x\n", __func__, i, download_block_size);
            goto program_device_cleanup;
        }

        iterations = 0;
        do {
            unsigned int wait_time;
            if (!issue_get_dfu_status_usb_req(dev, &dfu_status)) {
                dev_err(&dev->interface->dev, "%s: DFU_GetStatus failed block"
                        "=0x%X blocksize=0x%X\n",__func__, i,
                        download_block_size);
                goto program_device_cleanup;
            }

            if (dfu_status.status != DFU_STATUS_OK) {
                dev_err(&dev->interface->dev, "%s: Device not ok after "
                        "Download, address: 0x%x, dfu_status:0x%x\n",
                        __func__,
                        app_sector_start_address + (i * download_block_size),
                        dfu_status.status);
                issue_dfu_clear_status_usb_req(dev);
                goto program_device_cleanup;
            }

            wait_time = dfu_status.poll_timeout[0] |
                        (dfu_status.poll_timeout[1] << 8) |
                        (dfu_status.poll_timeout[2] << 16);

            dev_err(&dev->interface->dev, "%s: Download request waittime=%u\n",
                     __func__, wait_time);
            if (0 == wait_time)
                break;

            iterations++;
            if (iterations > GET_STATUS_MAX_ITERATIONS) {
                dev_err(&dev->interface->dev, "%s:Error:Get status returned "
                        "non zero wait for max times, waittime=%u\n",
                        __func__, wait_time);
                issue_dfu_clear_status_usb_req(dev);
                goto program_device_cleanup;
            }
            if (wait_event_interruptible_timeout(dev->download_wait,
                                            dev->dfu_device_removed,
                                            msecs_to_jiffies(wait_time))) {
                dev_err(&dev->interface->dev, "%s: Failed or halted wait, "
                        "waittime=%u\n", __func__, wait_time);
                //try to clear status, then quit
                issue_dfu_clear_status_usb_req(dev);
                goto program_device_cleanup;
            }
        }while(true);
    }

    if (!issue_dfu_download_usb_req(dev, 0, NULL, 0)) {
        dev_err(&dev->interface->dev, "%s: DFU_DOWNLOAD Failed to send zero "
                "length packet which transitions into manifest state\n",
                __func__);
        goto program_device_cleanup;
    }

    if (!issue_get_dfu_state_usb_req(dev, &dfu_state)) {
        dev_err(&dev->interface->dev, "%s: DFU_GetState Done programming check"
                " state failed\n", __func__);
        goto program_device_cleanup;
    }

    if (dfu_state != DFU_STATE_DFUMANIFESTSYNC) {
        dev_err(&dev->interface->dev, "%s: Expecting to be in Manifest sync "
                "state, state found in 0x%x\n",__func__, dfu_state);
        goto program_device_cleanup;
    }

    if (!issue_get_dfu_status_usb_req(dev, &dfu_status)) {
        dev_err(&dev->interface->dev, "%s: DFU_GetStatus failed block=0x%x "
                "blocksize=0x%x\n",__func__, i, download_block_size);
        goto program_device_cleanup;
    }
    dev_info(&dev->interface->dev, "%s:Successfully flashed firmware\n",
             __func__);
    retval = true;

program_device_cleanup:
    if (!retval) {
        if (mutex_is_locked(&dev->download_mutex))
            mutex_unlock(&dev->download_mutex);
        issue_dfu_abort_usb_req(dev);
    }
    return retval;
}

/*++

    dfu_download_thread

Routine Description:

    Kernel thread which will download firmware

Arguments:

    data - Pointer to device structure

Return Value:

    0 on success else error code

--*/

static int dfu_download_thread(void *data)
{
    int retval = -EINVAL;
    bool firmware_flashed = false;
    struct dfu_device *dev = (struct dfu_device *)data;

    if (!data)
        return retval;

    while(!kthread_should_stop()) {
        if (!firmware_flashed) {
            retval = download_program_device(dev);
            if (!retval)
                dev_warn(&dev->interface->dev, "DownloadProgramdevice "
                        "failed\n");
            else
                firmware_flashed = true;
        }
        msleep(10); //should not break until we receive kthread stop event
    }
   return 0;
}

static int pic_dfu_probe(struct usb_interface *interface,
                const struct usb_device_id *id)
{
    struct usb_host_interface *iface_desc = interface->cur_altsetting;
    struct dfu_device *dev = NULL;
    int retval = 0;

    dev_dbg(&interface->dev, "%s\n", __func__);
    if (!is_dfu_interface(iface_desc)) {
        dev_warn(&interface->dev, "%s: NOT DFU Interface, Exiting\n", __func__);
        retval = -ENODEV;
        goto cleanup;
    }

    //we found a device, interface and protocol, we can control, so create our
    //device context
    dev = kzalloc(sizeof(*dev), GFP_KERNEL);

    if (dev == NULL) {
          dev_err(&interface->dev, "Out of memory\n");
          retval = -ENOMEM;
          goto cleanup;
    }

    dev->interface = interface;
    dev->usb_dev = interface_to_usbdev(interface);
    init_waitqueue_head(&dev->download_wait);
    mutex_init(&dev->download_mutex);
    dev->dfu_device_removed = 0;
    usb_set_intfdata(interface, dev);
    dev->download_thread = kthread_run(dfu_download_thread, dev,
                                       "downloadThread");
    if (IS_ERR(dev->download_thread)) {
        retval = (int)PTR_ERR(dev->download_thread);
        dev_err(&dev->interface->dev, "%s: Failed to create/run Thread, "
                "return value:%d\n", __func__, retval);
        goto cleanup;
    }
    return 0;

cleanup:
    if (dev) {
        dfu_delete(dev);
    }
    return retval;
}

static void pic_dfu_disconnect(struct usb_interface *interface)
{
    struct dfu_device *dev;

    dev = usb_get_intfdata(interface);
    dev_dbg(&dev->interface->dev, "%s\n", __func__);
    dev->dfu_device_removed = 1;
    wake_up_interruptible(&dev->download_wait);
    dfu_delete(dev);
    usb_set_intfdata(interface, NULL);
}

static struct usb_driver pic_dfu_driver = {
        .name = "pic_dfu",
        .probe = pic_dfu_probe,
        .disconnect = pic_dfu_disconnect,
        .id_table = pic_dfu_table,
};

module_usb_driver(pic_dfu_driver);

MODULE_LICENSE("GPL");
MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESCRIPTION);
MODULE_VERSION(DRIVER_VERSION);

